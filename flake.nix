{
  description = "nix-utils";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
    in {
      formatter = pkgs.alejandra;
    })
    // {
      wrapFakeHome = {
        pkgs,
        package,
        binName,
        files,
      }: let
        filenames = builtins.attrNames files;
        mkDirTree = filename: "mkdir -p $(dirname ${filename})";
        mkDirTrees = builtins.concatStringsSep "\n" (builtins.map mkDirTree filenames);
        mkLink = filepath: contents: "ln -sf ${contents} ${filepath}";
        mkLinks = builtins.concatStringsSep "\n" (builtins.attrValues (builtins.mapAttrs mkLink files));
      in
        pkgs.writeShellScriptBin binName ''
          TMPD=$(mktemp -d)
          cd $TMPD
          ${mkDirTrees}
          ${mkLinks}
          echo "Emulated \$HOME: $TMPD"
          ls -alR $TMPD
          HOME=$TMPD ${package}/bin/${binName} $@
        '';
    };
}
